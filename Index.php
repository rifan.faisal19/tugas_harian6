<?php


require ("frog.php");
require ("ape.php");

echo "<h1> Halo..! Ini Tugas Harian ke 6 - OOP PHP</h1>";

$hewan = new animal("Domba");

echo "Name : " . $hewan->binatang . "<br>";
echo "Legs : ". $hewan->legs . "<br>";
echo "Cold Blooded : " . $hewan->cold_blooded . "<br>";

echo "<br><br>";

$hewan2 = new frog ("Katak");

echo "Name : " . $hewan2->binatang . "<br>";
echo "Legs : " .$hewan2->legs . "<br>";
echo "Cold Blooded : " . $hewan2->cold_blooded . "<br>"; 
echo "Jump : " . $hewan2->jump . "<br>"; 


echo "<br><br>";

$hewan3 = new ape ("Kera Sakti Sungokong");

echo "Name : " . $hewan3->binatang . "<br>";
echo "Legs : " .$hewan3->legs . "<br>";
echo "Cold Blooded : " . $hewan3->cold_blooded . "<br>"; 
echo "Yell : " . $hewan3->yell . "<br>"; 
?>